/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjwstk.mpr.jdbcmaven.service;

import java.util.Arrays;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import org.junit.Test;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.Address;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.ClientDetails;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.Order;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.OrderItem;

/**
 *
 * @author radek
 */
public class OrderTest {
    
    OrderManager m = new OrderManager();
    
    private final static ClientDetails CLIENT = new ClientDetails("Józek", "Nazwiskowy", "joznaz");
    private final static Address ADDRESS = new Address("polandia", "miasto", "123-xx", "uliczna", "2312", "1234");
    private final static OrderItem ORDER_ITEM1 = new OrderItem("nazwa", "opis przedmiotu", 2.99);
    private final static OrderItem ORDER_ITEM2 = new OrderItem("kolejny produkt", "opis kolejnego przedmiotu", 10);
    private final static List <OrderItem> ITEMS = Arrays.asList(ORDER_ITEM1,ORDER_ITEM2);
    

    @Test
    public void checkConnection() {
        assertNotNull(m.getConnection());
    }
    
    @Test
    public void checkAdding() {
        Order o = new Order(CLIENT, ADDRESS, ITEMS);
        
        m.clearOrders();
        assertEquals(1, m.addOrder(o));
        
        List<Order> orders = m.getAllOrders();
        Order orderRetrived = orders.get(0);
        
        assertEquals(CLIENT, orderRetrived.getClient());
        assertEquals(ADDRESS, orderRetrived.getDeliveryAddress());
        assertEquals(ITEMS, orderRetrived.getItems());
    }
    
}
