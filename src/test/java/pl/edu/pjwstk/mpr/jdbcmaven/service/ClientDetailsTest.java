/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjwstk.mpr.jdbcmaven.service;

import java.util.List;
import junit.framework.Assert;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import org.junit.Test;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.ClientDetails;

/**
 *
 * @author radek
 */
public class ClientDetailsTest {

    ClientDetailsManager cm = new ClientDetailsManager();

    private final static String NAME_1 = "Zenon";
    private final static String SURNAME_1 = "Polak";
    private final static String LOGIN_1 = "zenopol";
    
    @Test
    public void checkConnection() {
        assertNotNull(cm.getConnection());
    }
    
    @Test
    public void checkAdding() {
        ClientDetails c = new ClientDetails(NAME_1, SURNAME_1, LOGIN_1);
        cm.clearClientDetails();
        assertEquals(1, cm.addClientDetails(c));
        
        List<ClientDetails> clients = cm.getAllClientsDetails();
        ClientDetails clientRetrived = clients.get(0);
        
        assertEquals(NAME_1, clientRetrived.getName());
        assertEquals(SURNAME_1, clientRetrived.getSurname());
        assertEquals(LOGIN_1, clientRetrived.getLogin());
    }
}
