/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjwstk.mpr.jdbcmaven.service;

import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import org.junit.Test;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.Address;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 *
 * @author radek
 */
public class AddressTest {

    AddressManager m = new AddressManager();

    private final static String COUNTRY = "Polska Ludowa";
    private final static String CITY = "Frei Stadt Danzig";
    private final static String POSTAL_CODE = "123-45";
    private final static String STREET = "Sezamkowa";
    private final static String BUILDING_NUMBER = "103";
    private final static String FLAT_NUMBER = "1";
    
//    m.dropTable();
    
    @Test
    public void checkConnection() {
        assertNotNull(m.getConnection());
    }
    
    @Test
    public void checkAdding() {
        Address a = new Address(COUNTRY, CITY, POSTAL_CODE, STREET, BUILDING_NUMBER, FLAT_NUMBER);
        
        
        m.clearAddresses();
        assertEquals(1, m.addAddress(a));
        
        List<Address> addresses = m.getAllAddresses();
        Address addressRetrived = addresses.get(0);
        
        assertEquals(COUNTRY, addressRetrived.getCountry());
        assertEquals(CITY, addressRetrived.getCity());
        assertEquals(POSTAL_CODE, addressRetrived.getPostalCode());
        assertEquals(STREET, addressRetrived.getStreet());
        assertEquals(BUILDING_NUMBER, addressRetrived.getBuildingNumber());
        assertEquals(FLAT_NUMBER, addressRetrived.getFlatNumber());
    }
}
