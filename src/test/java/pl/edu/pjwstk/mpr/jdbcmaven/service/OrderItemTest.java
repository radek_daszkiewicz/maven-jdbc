/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjwstk.mpr.jdbcmaven.service;

import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import org.junit.Test;
import pl.edu.pjwstk.mpr.jdbcmaven.domain.OrderItem;

/**
 *
 * @author radek
 */
public class OrderItemTest {
    
    OrderItemManager m = new OrderItemManager();
    
    private final static String NAME = "Produkt";
    private final static String DESCRIPTION = "opis produktu";
    private final static Double PRICE = 999.99;
    
    @Test
    public void checkConnection() {
        assertNotNull(m.getConnection());
    }
    
    @Test
    public void checkAdding() {
        OrderItem i = new OrderItem(NAME, DESCRIPTION, PRICE);
        
        m.clearOrderItems();
        assertEquals(1, m.addOrderItem(i));
        
        List<OrderItem> items = m.getAllOrderItems();
        OrderItem itemRetrived = items.get(0);
        
        assertEquals(NAME, itemRetrived.getName());
        assertEquals(DESCRIPTION, itemRetrived.getDescription());
        assertEquals(PRICE, itemRetrived.getPrice());
    }
}
