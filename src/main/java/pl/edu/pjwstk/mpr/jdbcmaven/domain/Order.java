/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pjwstk.mpr.jdbcmaven.domain;

import java.util.List;

/**
 *
 * @author radek
 */
public class Order {
    private long id;
    private ClientDetails client;
    private Address deliveryAddress;
    private List<OrderItem> items;

    public Order(ClientDetails client, Address deliveryAddress, List<OrderItem> items) {
        super();
        this.client = client;
        this.deliveryAddress = deliveryAddress;
        this.items = items;
    }

    public Order() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }
    
}